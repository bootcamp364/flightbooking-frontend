import React, { FC } from 'react';
import ReactLoading from 'react-loading';
import './loader.scss';

type LoadingType =
  | 'blank'
  | 'balls'
  | 'bars'
  | 'bubbles'
  | 'cubes'
  | 'cylon'
  | 'spin'
  | 'spinningBubbles'
  | 'spokes';

interface propsInterface {
  type?: LoadingType;
  color?: string;
}

const FullPageLoader: FC<propsInterface> = ({
  type = 'spinningBubbles',
  color = '#f77728',
}) => (
  <div className="fullPage_loader_container">
    <ReactLoading type={type} color={color} height={50} width={50} />
  </div>
);

export default FullPageLoader;
