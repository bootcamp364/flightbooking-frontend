export interface FlightDetailInterface {
  airlineName: string;
  flightNumber: string;
  fromDate: string;
  fromPlace: string;
  fromTerminal: string;
  price: string;
  toDate: string;
  toPlace: string;
  toTerminal: string;
  _id?: string;
  isDetailPage?: boolean;
}

export interface FormvaluesInterface {
  origin?: string;
  dest?: string;
  date?: string;
}

export interface TravellerDetailCardPropsInterface {
  name?: string;
  email?: string;
  number?: string;
}
